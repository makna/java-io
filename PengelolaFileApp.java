public class PengelolaFileApp {
    public static void main(String[] args) {
        PengelolaFile pengelolaFile = new PengelolaFile();
        // baca isi file
        String filename = "/Users/hasyim/Documents/latihan/java_io/resources/contoh_file.txt";
        pengelolaFile.bacaIsiFile(filename);

        // tulis isi file
        // String contents = "Isi file yang baru terdiri dari beberapa baris\n"
        //     + "baris pertama terdiri dari beberapa kata\n"
        //     + "baris kedua terdiri dari dari beberapa kata juga\n"
        //     + "baris ketiga dan yang terakhir.\n" ;
        // pengelolaFile.tulisIsiFile(filename, contents);

        // baca isi image
        // String imagefile = "resources/talk_icon.png";
        // pengelolaFile.bacaIsiImage(imagefile);
    }
}