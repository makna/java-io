import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class PengelolaFile {

    // function untuk baca isi file
    public void bacaIsiFile(String filename) {
        FileReader fileReader = null;
        BufferedReader bufferdReader = null;
        try {
            fileReader = new FileReader(filename);
            // int data;
            // while ((data = fileReader.read()) != -1) {
            //     // Thread.sleep(1 * 1000);
            //     System.out.print((char)data);
            // }

            bufferdReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferdReader.readLine()) != null) {
                System.out.println(line);
            }
            
        } catch (FileNotFoundException e) {
            // e.printStackTrace();
            System.out.println("ERROR: file " + filename + " tidak ditemukan.");
        } catch (IOException e) {
            // e.printStackTrace();
            System.out.println("ERROR: pada saat baca file");
        } 
        // catch (InterruptedException e) {
        //     e.printStackTrace();
        // } 
        finally {
            // close file reader
            try {
                bufferdReader.close();
                fileReader.close();
            } catch (Exception e) {}
        }
    }

    public void tulisIsiFile(String filename, String contents) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(filename);
            fileWriter.write(contents);
        } catch (IOException e) {
            // e.printStackTrace();
            System.out.println("ERROR: tulis file " + filename );
        } finally {
            try {
                fileWriter.close();
            } catch (Exception e) {}
        }
    }

    public void bacaIsiImage(String filename) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(filename);
            int data;
            while((data = fileInputStream.read()) != -1){
                System.out.print(data);
            }
        } catch (FileNotFoundException e) {
            // e.printStackTrace();
            System.out.println("ERROR: file " + filename + " tidak ditemukan.");
        } catch (IOException e) {
            // e.printStackTrace();
            System.out.println("ERROR: pada saat baca file");
        } 

    }

}
